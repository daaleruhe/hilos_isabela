from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from .models import Producto
from .forms import ProductoForm

class ProductoList(ListView):
	model = Producto
	template_name = 'productos/lista_producto.html'

class ProductoDetail(DetailView):
	model = Producto
	template_name = 'productos/detalle_producto.html'

class ProductoCreate(CreateView):
	model = Producto
	form_class = ProductoForm
	success_url = reverse_lazy('producto:producto-list')
	template_name = 'productos/create_update_producto.html'

class ProductoUpdate(UpdateView):
	model = Producto
	form_class = ProductoForm
	success_url = reverse_lazy('producto:producto-list')
	template_name = 'productos/create_update_producto.html'

class ProductoDelete(DeleteView):
	model = Producto
	success_url = reverse_lazy('producto:producto-list')
	template_name = 'productos/eliminar_producto.html'
