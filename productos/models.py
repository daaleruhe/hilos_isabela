from django.db import models

from marcas.models import Marca
from tipo_producto.models import Tipo_producto

# Create your models here.

class Producto(models.Model):
    """tp = [
        ( 0 , 'Hilo'),
        ( 1 , 'Aguja'),
        ( 2 , 'Algodón'),
        ( 3 , 'Seda'),
        ( 4 , 'Cuero'),
        ( 5 , 'Poliéster'),
        ( 6 , 'Nylon')
    ]"""

    referencia = models.CharField(
        max_length=30, 
        unique=True, 
        help_text="Ingresar la referencia del producto")

    nombre = models.CharField(
        max_length=50, 
        help_text="Ingresar el nombre del producto")

    precio = models.IntegerField(
        help_text="Ingresar el precio del producto")

    cantidad = models.IntegerField(
        help_text="Ingresar la cantidad del producto")

    estado = models.BooleanField(
        "Habilitado", 
        default=True)

    marca = models.ForeignKey(
        Marca, 
        on_delete=models.CASCADE, 
        help_text="Ingrese la marca del producto")

    tipo_de_producto = models.ForeignKey(
        Tipo_producto, 
        on_delete=models.CASCADE, 
        help_text="Ingrese la tipo de producto del producto")

    """tipo_de_producto = models.SmallIntegerField(choices = tp, help_text="Ingresar el tipo del producto")"""
    

    def mostrar_estado(self):
        if self.estado:
            mostrar_estado = "Habilitado"
        else:
            mostrar_estado = "Inhabilitado"
        
        return mostrar_estado
    
    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ('estado', 'precio', )
    