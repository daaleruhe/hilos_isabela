from django.urls import path

from .views import ProductoList, ProductoDetail, ProductoCreate, ProductoUpdate, ProductoDelete

app_name = 'producto'
urlpatterns = [
    path('', ProductoList.as_view(), name='producto-list'),
    path('detail/<int:pk>/', ProductoDetail.as_view(), name='detail'),
    path('create/', ProductoCreate.as_view(), name='create'),
    path('update/<int:pk>/', ProductoUpdate.as_view(), name='update'),
    path('delete/<int:pk>/', ProductoDelete.as_view(), name='delete'),
]