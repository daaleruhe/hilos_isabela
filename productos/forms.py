from django import forms
from django.forms import ModelForm
from .models import Producto

class ProductoForm(ModelForm):

	class Meta:
		model = Producto
		fields = '__all__'
		exclude = ('estado',)

	"""marca = forms.CharField(widget=forms.TextInput(attrs={
		'placeholder': 'Marca',
		'class': 'form-control',}))"""

	