from django.urls import path

from .views import MarcaList, MarcaCreate, MarcaDetail, MarcaUpdate, MarcaDelete

app_name = 'marca'
urlpatterns = [
    path('', MarcaList.as_view(), name='marca-list'),
    path('detail/<int:pk>/', MarcaDetail.as_view(), name='detail'),
    path('create/', MarcaCreate.as_view(), name='create'),    
    path('update/<int:pk>/', MarcaUpdate.as_view(), name='update'),
    path('delete/<int:pk>/', MarcaDelete.as_view(), name='delete'),
]