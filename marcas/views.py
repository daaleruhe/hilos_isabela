from django.shortcuts import render
from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView
from django.urls import reverse_lazy

from .models import Marca
from .forms import MarcaForm

class MarcaList(ListView):
	model = Marca
	template_name = 'marcas/lista_marca.html'
	
class MarcaDetail(DetailView):
	model = Marca
	template_name = 'marcas/detalle_marca.html'

class MarcaCreate(CreateView):
	model = Marca
	form_class = MarcaForm
	success_url = reverse_lazy('marca:marca-list')
	template_name = 'marcas/create_update_marcas.html'

class MarcaUpdate(UpdateView):
	model = Marca
	form_class = MarcaForm
	success_url = reverse_lazy('marca:marca-list')
	template_name = 'marcas/create_update_marcas.html'

class MarcaDelete(DeleteView):
	model = Marca
	success_url = reverse_lazy('marca:marca-list')
	template_name = 'marcas/eliminar_marca.html'