from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from .models import Cliente
from .forms import ClienteForm

class ClienteList(ListView):
	model = Cliente
	template_name = 'clientes/lista_cliente.html'

class ClienteDetail(DetailView):
	model = Cliente
	template_name = 'clientes/detalle_clientes.html'

class ClienteCreate(CreateView):
	model = Cliente
	form_class = ClienteForm
	success_url = reverse_lazy('cliente:cliente-list')
	template_name = 'clientes/create_update_clientes.html'

class ClienteUpdate(UpdateView):
	model = Cliente
	form_class = ClienteForm
	success_url = reverse_lazy('cliente:cliente-list')
	template_name = 'clientes/create_update_clientes.html'

class ClienteDelete(DeleteView):
	model = Cliente
	success_url = reverse_lazy('cliente:cliente-list')
	template_name = 'clientes/eliminar_cliente.html'
