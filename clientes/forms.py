from django import forms
from django.forms import ModelForm
from django.core.exceptions import ValidationError

from .models import Cliente

class ClienteForm(ModelForm):
    class Meta:
        model = Cliente
        fields = '__all__'

    fields_only_letters = [
        'primer_nombre', 'segundo_nombre', 
        'primer_apellido', 'segundo_apellido'
    ]

    documento_identidad = forms.CharField(widget=forms.NumberInput(attrs={
        'placeholder': 'DNI',
        'size': '8'}))
    primer_nombre = forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Nombre Cliente',
        'size': '40',
        'pattern': "[-a-zA-Z_]+"}))	
    primer_apellido = forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Apellidos',
        'size': '40'}))		
    direccion = forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Direccion',
        'size': '40'}))	
    telefono = forms.CharField(widget=forms.NumberInput(attrs={
        'placeholder': 'Telefono',
        'size': '8'}))

    """def clean_documento_identidad(self):
        diccionario_limpio = self.cleaned_data
        documento_identidad = diccionario_limpio.get('documento_identidad')
        if len(documento_identidad) < 8 or len(documento_identidad) > 8:
            raise forms.ValidationError("debe ser de 8 Digitos")
        return documento_identidad"""

    def nombre(self):
        primer_nombre = self.cleaned_data['primer_nombre']
        if not primer_nombre.isalpha():
            raise forms.ValidationError('No puede contener números')
        return primer_nombre

    """def __init__(self, *args, **kwargs):
        super(ClienteForm, self).__init__(*args, **kwargs)

        for visible in self.visible_fields():
            attrs = {'class': 'form-control', 'placeholder': visible.label}
            if visible.name in self.fields_only_letters:
                attrs.update({'pattern': "[-a-zA-Z_]+"})

            visible.field.widget.attrs.update(attrs)

            if self.errors.get(visible.name, None):
                visible.field.widget.attrs.update({'class': 'form-control is-invalid'})"""