from django.urls import path

from .views import ClienteList, ClienteCreate, ClienteUpdate, ClienteDetail, ClienteDelete

app_name = 'cliente'
urlpatterns = [
    path('', ClienteList.as_view(), name='cliente-list'),
    path('detail/<int:pk>/', ClienteDetail.as_view(), name='detail'),
    path('create/', ClienteCreate.as_view(), name='create'),
    path('update/<int:pk>/', ClienteUpdate.as_view(), name='update'),
    path('delete/<int:pk>/', ClienteDelete.as_view(), name='delete'),
]