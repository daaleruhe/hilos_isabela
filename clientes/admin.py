from django.contrib import admin
from .models import Cliente

@admin.register(Cliente)
class ClienteAdmin(admin.ModelAdmin):
	list_display = (
        'documento_identidad',
        'primer_nombre',
        'segundo_nombre', 
        'primer_apellido',
        'segundo_apellido',
        'direccion',
        'telefono'  )

	search_fields = (
        'documento_identidad',
        'primer_nombre',
        'segundo_nombre', 
        'primer_apellido',
        'segundo_apellido')