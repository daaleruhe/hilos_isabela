from django.db import models

from .validators import validate_only_letters

class Cliente(models.Model):

    """tipo_cl = [
        ( 0 , 'Independiente'),
        ( 1 , 'Empresa'),
        ( 2 , 'Local')
    ]"""

    """tipo_de_cliente= models.SmallIntegerField(
		choices=tipo_cl, 
		help_text="Ingrese el tipo del cliente"
	)"""

    """tipo_de_documento = models.SmallIntegerField(
		choices=tipo_id, 
		help_text="Ingresar el tipo de documento de identidad"
	)"""

    documento_identidad = models.IntegerField(
        unique=True,
        help_text="Ingresar solo números")

    primer_nombre = models.CharField(
        max_length=40,
        validators=[validate_only_letters],
        help_text="Ingresar solo letras")

    segundo_nombre = models.CharField(
        max_length=40, blank=True,
        validators=[validate_only_letters],
        null=True,
        help_text="Ingresar solo letras")

    primer_apellido = models.CharField(
        validators=[validate_only_letters],
        max_length=40,
        help_text="Ingresar solo letras")

    segundo_apellido = models.CharField(
        max_length=40,
        validators=[validate_only_letters],
        help_text="Ingresar solo letras")
    
    correo_electronico = models.EmailField(
		max_length=100, 
		null=True, 
		blank=True, 
		unique=True, 
		help_text="Ingresa un dirección de correo valida")

    direccion = models.CharField(
        max_length=35,
        help_text="Ingresar una dirección valida")

    telefono = models.IntegerField(
        help_text="Ingresar solo números")

    def nombre_completo(self):
        if self.segundo_nombre:
            nombre = "{0} {1} {2} {3}".format(
                self.primer_nombre,
                self.segundo_nombre,
                self.primer_apellido,
                self.segundo_apellido)
        else:
            nombre = "{0} {1} {2}".format(
                self.primer_nombre,
                self.primer_apellido,
                self.segundo_apellido)
        return nombre


    def __str__(self):
        return self.nombre_completo()