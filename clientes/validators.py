import re

from django.core.validators import RegexValidator
from django.utils.translation import gettext_lazy as _

#letter_re = r'^[-a-zA-Z_]+\Z'
letter_re = r'^\w+\Z'
validate_only_letters = RegexValidator(
    letter_re,
    # Translators: "letters" means latin letters: a-z and A-Z.
    _("Valores invalidos"),
    'invalid'
)