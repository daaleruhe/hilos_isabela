from django.contrib import admin
from .models import Venta, ProductosVendidos


__all__ = ('ProductosVendidosStackedInline', )


class ProductosVendidosStackedInline(admin.StackedInline):
    model = ProductosVendidos


@admin.register(Venta)
class VentaAdmin(admin.ModelAdmin):
    inlines = (ProductosVendidosStackedInline, )

