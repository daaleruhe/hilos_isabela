from django.views.generic import CreateView
from .models import Venta


class VentaView(CreateView):
    model = Venta
