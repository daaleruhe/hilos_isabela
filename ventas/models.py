from django.conf import settings
from django.db import models
from django.db.models import signals

from productos.models import Producto
from clientes.models import Cliente


__all__ = ('Venta', 'ProductosVendidos')


class Venta(models.Model):
    productos = models.ManyToManyField(
        Producto,
        through='ProductosVendidos',
    )
    vendedor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)


class ProductosVendidos(models.Model):
    producto = models.ForeignKey(
        Producto,
        on_delete=models.CASCADE,
    )
    venta = models.ForeignKey(
        Venta,
        on_delete=models.CASCADE,
    )
    quantity = models.PositiveSmallIntegerField()


def update_stock(sender, instance, **kwargs):
    print(instance.quantity, instance.producto.cantidad)
    instance.producto.cantidad -= instance.quantity
    print('==> '*2, instance.producto.cantidad)
    instance.producto.save()


# register the signal
signals.post_save.connect(update_stock, sender=ProductosVendidos, dispatch_uid="update_stock_count")
