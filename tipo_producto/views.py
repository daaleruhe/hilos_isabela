from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView


from .models import Tipo_producto
from .forms import TipoProductoForm

class TipoProductoList(ListView):
	model = Tipo_producto
	template_name = 'tipos_productos/lista_tipo_producto.html'

class TipoProductoDetail(DetailView):
	model = Tipo_producto
	template_name = 'tipos_productos/detalle_tipo_producto.html'

class TipoProductoCreate(CreateView):
	model = Tipo_producto
	form_class = TipoProductoForm
	success_url = reverse_lazy('tipo_producto:tipo_producto-list')
	template_name = 'tipos_productos/create_update_tipo_producto.html'

class TipoProductoUpdate(UpdateView):
	model = Tipo_producto
	form_class = TipoProductoForm
	success_url = reverse_lazy('tipo_producto:tipo_producto-list')
	template_name = 'tipos_productos/create_update_tipo_producto.html'
	
class TipoProductoDelete(DeleteView):
	model = Tipo_producto
	success_url = reverse_lazy('tipo_producto:tipo_producto-list')
	template_name = 'tipos_productos/eliminar_tipo_producto.html'