from django.urls import path

from .views import TipoProductoList, TipoProductoCreate, TipoProductoDetail, TipoProductoUpdate, TipoProductoDelete

app_name = 'tipo_producto'
urlpatterns = [
    path('', TipoProductoList.as_view(), name='tipo_producto-list'),    
    path('detail/<int:pk>/', TipoProductoDetail.as_view(), name='detail'),
    path('create/', TipoProductoCreate.as_view(), name='create'),
    path('update/<int:pk>/', TipoProductoUpdate.as_view(), name='update'),
    path('delete/<int:pk>/', TipoProductoDelete.as_view(), name='delete'),
]