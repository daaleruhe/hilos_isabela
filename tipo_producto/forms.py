from django.forms import ModelForm

from .models import Tipo_producto

class TipoProductoForm(ModelForm):
    class Meta:
        model = Tipo_producto
        fields = '__all__'
