from django.apps import AppConfig


class TipoProductoConfig(AppConfig):
    name = 'tipo_producto'
